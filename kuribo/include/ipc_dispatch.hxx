#ifndef NSK_IPCDISPATCH_H
#define NSK_IPCDISPATCH_H

#include "i_ipc_handler.hxx"
#include <map>
#include <string>

namespace kuribo {

class IpcDispatch {
public:
  IpcDispatch ();
  virtual ~IpcDispatch (){};
  void listen();
  void on_handler_done(picojson::value returnObj);
private:
  std::map<std::string, IIpcHandlerP> _dispatchMap;
};

} /* kuribo */

#endif /* end of include guard: NSK_IPCDISPATCH_H */
