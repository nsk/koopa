#ifndef NSK_SCANFILEIH_H
#define NSK_SCANFILEIH_H

#include "i_ipc_handler.hxx"
#include "picojson.h"

namespace kuribo {

class ScanFileIH : public IIpcHandler {
public:
  ScanFileIH(IpcDispatch* dispatch) : IIpcHandler(dispatch){}
  void exec(picojson::value& args);
};

} /* kuribo */

#endif /* end of include guard: NSK_SCANFILEIH_H */
