#ifndef NSK_IIPCHANDLER_H
#define NSK_IIPCHANDLER_H

#include "picojson.h"
#include <memory>


namespace kuribo {

class IpcDispatch;

class IIpcHandler {
public:
  IIpcHandler(IpcDispatch* dispatch) : _dispatch(dispatch){}
  virtual void exec(picojson::value& args) = 0;
protected:
  IpcDispatch* _dispatch;
};

typedef std::shared_ptr<IIpcHandler> IIpcHandlerP;
} /* kuribo */

#endif /* end of include guard: NSK_IIPCHANDLER_H */
