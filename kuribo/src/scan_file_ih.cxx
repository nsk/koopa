#include "scan_file_ih.hxx"
#include "ipc_dispatch.hxx"
#include "picojson.h"
#include <iostream>
#include <fstream>
#include <map>
#include <string>

using namespace picojson;
using namespace std;

bool file_exists(string ff) {
  std::ifstream infile(ff);
  return infile.good();
}

void kuribo::ScanFileIH::exec(picojson::value& args) {
  auto ff = args.get<object>()["file"].get<string>();
  cout << "tostr: " << args.to_str() << endl;
  cout << "ScanFileIH::exec::file -> " << ff << endl;
  cout << "file exists: " << file_exists(ff) << endl;

  auto reto = object();
  reto["argone"] = value("erik");
  reto["argtwo"] = value("maria");
  _dispatch->on_handler_done(value(reto));
}
