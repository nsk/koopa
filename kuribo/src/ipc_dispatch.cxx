#include "ipc_dispatch.hxx"
#include "scan_file_ih.hxx"
#include "picojson.h"
#include <memory>
#include <iostream>
#include <string>
#include <sstream>

using namespace std;
using namespace picojson;

#define IPC_RET_TAG "NSKIPC::RET::"

kuribo::IpcDispatch::IpcDispatch() {
  _dispatchMap["scan_file"] = std::shared_ptr<ScanFileIH>(new ScanFileIH(this));
}

void kuribo::IpcDispatch::listen() {
  string linein;
  cin >> linein;

  picojson::value v;
  std::string err = picojson::parse(v, linein);
  if (! err.empty()) {
    std:cerr << err << std::endl;
    throw err;
  }

  cout << "got value: " << v.to_str() << endl;

  string method = v.get<object>()["method"].get<string>();
  cout << "execcing method: " << method << endl;

  _dispatchMap[method]->exec(v.get<object>()["args"]);
}

void kuribo::IpcDispatch::on_handler_done(picojson::value retVal) {
  cout << IPC_RET_TAG << retVal << endl;
}
