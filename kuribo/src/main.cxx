#include "ipc_dispatch.hxx"
#include "tclap/CmdLine.h"
#include <iostream>

using namespace std;
using namespace kuribo;

int main(int argc, char const *argv[]) {
  try {
    TCLAP::CmdLine cmd("A cli frontend for libkuribo.", ' ', "0.1.0");
    TCLAP::SwitchArg ipcSwitch("", "ipc", "Executes kuribo-cli in ipc mode", cmd, false);
    cmd.parse(argc, argv);

    if(ipcSwitch.getValue()) {
      IpcDispatch dis;
      dis.listen();
      cout << "finished duder." << endl;
    } else {
      cerr << "ERROR: Non-ipc mode is not yet implemented" << endl;
      return 1;
    }
  } catch(TCLAP::ArgException &e) {
    cerr << "Error happened while parsing the commands." << e.error() << endl;
    return 1;
  }
}
