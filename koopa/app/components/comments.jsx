'use strict';

import React from 'react';
import ReactDom from 'react-dom';
import server from '../../util/mock_server';

const Comment = React.createClass({
  render: function () {
    return (
      <div className="comment">
        <h2 className="commentAuthor">
          {this.props.author}
        </h2>
        {this.props.children.toString()}
      </div>
    );
  },
});

const CommentList = React.createClass({
  render: function () {
    const commentNodes = this.props.data.map((comment) => {
      return (
        <Comment author={comment.author} key={comment.id}>
          {comment.text}
        </Comment>
      );
    });

    return (
      <div className="commentList">
        {commentNodes}
      </div>
    );
  },
});

const CommentForm = React.createClass({
  getInitialState: function (attribute) {
    return { author: '', text: '' };
  },
  handleAuthorChange: function (e) {
    this.setState({author: e.target.value});
  },
  handleTextChange: function (e) {
    this.setState({text: e.target.value});
  },
  handleSubmit: function (e) {
    e.preventDefault();
    let author = this.state.author.trim();
    let text = this.state.text.trim();
    if(!text || !author) {
      return;
    }
    this.setState({author: '', text: ''})
    this.props.onCommentSubmit({author: author, text: text});
  },
  render: function () {
    return (
      <form className="commentForm" onSubmit={this.handleSubmit}>
        <input
          type="text"
          placeholder="Your name"
          value={this.state.author}
          onChange={this.handleAuthorChange}
        />
        <input
          type="text"
          placeholder="Say something..."
          value={this.state.text}
          onChange={this.handleTextChange}
        />
        <input type="submit" placeholder="Post" />
      </form>
    );
  },
});

const CommentBox = React.createClass({
  loadComments: function () {
    server.getData(this.props.url).then((data) => {
      this.setState({data: data});
    });
  },
  getInitialState: function () {
    return { data: [] };
  },
  componentDidMount: function () {
    this.loadComments();
  },
  handleCommentSubmit: function (newComment) {
    console.log('CommentBox::handleCommentSubmit');
    server.postData(newComment).then((data) => {
      this.setState({data: data});
    });
  },
  render: function () {
    return (
      <div className="commentBox">
        <h1>Comments</h1>
        <CommentList data={this.state.data}/>
        <CommentForm onCommentSubmit={this.handleCommentSubmit} />
      </div>
    );
  },
});

export {
  CommentBox
}
