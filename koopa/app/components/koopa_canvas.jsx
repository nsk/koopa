'use strict';

import React, { Component, PropTypes } from 'react';
import pixi from "pixi.js";

export default class KoopaCanvas extends Component {
  constructor(props) {
    super(props);

    // Prebind some methods
    this.animate = this.animate.bind(this);
    this.updateZoomLevel = this.updateZoomLevel.bind(this);
  }
  componentDidMount() {
    // Here, componentDidMount is used to grab the canvas container ref,
    // and hook up the PixiJS renderer
    this.renderer = pixi.autoDetectRenderer(800, 600);
    this.refs.gameCanvas.appendChild(this.renderer.view);

    // Create the root of the scene graph
    this.stage = new pixi.Container();
    this.stage.width = 800;
    this.stage.height = 600;

    // Start the pixi loop
    this.animate();
  }
  // shouldComponentUpdate(nextProps, nextState) {
  //   // Used to check our new props against the current and only update
  //   // when needed.
  //   console.log('shouldComponentUpdate::nextProps');
  //   console.log(nextProps);
  //   return nextProps.zoomLevel !== this.props.zoomLevel;
  // }
  componentWillReceiveProps(nextProps) {
    // When we get new props, run the appropriate imperative functions
    console.log('componentWillReceiveProps::nextProps');
    if(nextProps.zoomLevel !== this.props.zoomLevel) {
      this.updateZoomLevel(nextProps);
    }
    if(nextProps.spriteSheet !== this.props.spriteSheet) {
      this.renderSpriteSheet(nextProps.spriteSheet);
    }
    this.drawRect(nextProps.rect);
  }
  updateZoomLevel(props) {
    console.log('updating zoom level')
    this.stage.scale.x = props.zoomLevel;
    this.stage.scale.y = props.zoomLevel;
  }
  drawRect(rect) {
    console.log(`drawing rect: ${rect}`);
    let graphics = new pixi.Graphics();
    graphics.lineStyle(1, 0xFF0000);
    graphics.drawRect(rect[0], rect[1], rect[2], rect[3]);
    this.stage.addChild(graphics)
  }
  renderSpriteSheet(spriteSheet) {
    console.log('Rendering spritesheet');
    console.log(spriteSheet);
    let texture = pixi.Texture.fromImage(spriteSheet);
    let sheetSprite = new pixi.Sprite(texture);

    this.stage.addChild(sheetSprite);
  }
  animate() {
    // render the stage container
    this.renderer.render(this.stage);
    this.frame = requestAnimationFrame(this.animate);
  }
  render() {
    // Render our container that will store our PixiJS canvas
    return (
      <div className="koopa-canvas-container" ref="gameCanvas"></div>
    );
  }
}

KoopaCanvas.propTypes = {
  zoomLevel: PropTypes.number.isRequired
};
