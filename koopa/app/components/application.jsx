'use strict';

import React, { Component } from 'react';
import KoopaCanvas from './koopa_canvas.jsx';
import FilePicker from './file_picker.jsx';

export default class Application extends Component {
  constructor(props) {
    super(props);

    this.state = {
      zoomLevel: 1.0,
      spriteSheet: null,
      rect: null
    };

    // Prebind the handler
    this.onDebugClick = this.onDebugClick.bind(this);
    this.onZoomIn = this.onZoomIn.bind(this);
    this.onZoomOut= this.onZoomOut.bind(this);
    this.onFilePicked = this.onFilePicked.bind(this);
  }
  onDebugClick() {
    console.log('Application::onDebugClick');
    this.setState({
      rect: [100, 100, 100, 100]
    });
  }
  onZoomIn() {
    let zoomLevel = this.state.zoomLevel += 0.1;
    this.setState({zoomLevel});
  }
  onZoomOut() {
    let zoomLevel = this.state.zoomLevel -= 0.1;
    if(zoomLevel >= 0) {
      this.setState({zoomLevel});
    }
  }
  onFilePicked(newSpriteSheet) {
    console.log(`Application::onFilePicked`);
    this.setState({spriteSheet: newSpriteSheet});
  }
  render() {
    return (
      <div className="application container">
        <button onClick={this.onDebugClick}>Debug</button>
        <button onClick={this.onZoomIn}>Zoom In</button>
        <button onClick={this.onZoomOut}>Zoom Out</button>
        <button onClick={this.onZoomOut}>Zoom Out</button>
        <KoopaCanvas
          zoomLevel={this.state.zoomLevel}
          spriteSheet={this.state.spriteSheet}
          rect={this.state.rect}
        />
        <FilePicker onFilePicked={this.onFilePicked}/>
      </div>
    );
  }
}
