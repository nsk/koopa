'use strict';

import React, { Component } from 'react';

export default class FilePicker extends Component {
  constructor(props) {
    super(props);

    // Prebind the handlers
    this.pickFile = this.pickFile.bind(this);
  }
  pickFile() {
    console.log('FilePicker::pickFile');

    let val = window.edialog.showOpenDialog({
      properties: ['openFile']
    });

    if(val) {
      this.props.onFilePicked(val[0]);
    }
  }
  render() {
    return (
      <div>
        <button onClick={this.pickFile}>Pick File</button>
      </div>
    );
  }
}
