'use strict';

import React from 'react';
import ReactDOM from 'react-dom';
import Application from './components/application.jsx';

ReactDOM.render(
  <Application />,
  document.getElementById('application-root')
)
