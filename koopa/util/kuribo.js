'use strict';

const spawn = require('child_process').spawn;

export default class Kuribo {
  constructor(kcli_exe) {
    console.log(`instantiating kuribo with kcli_exe: ${kcli_exe}`);
    this.kcli_exe = kcli_exe;
  }
  scanFile(file) {
    console.log(`Requesting scan of file: ${file}`);
    return new Promise(function(resolve, reject) {

      setTimeout(function () {
        resolve('resolved value!');
      }, 2000);

      // const kcli = spawn(kuribo_cli, ['--ipc']);
      // kcli.stdout.setEncoding('utf8');
      //
      // kcli.stdout.on('data', (data) => {
      //   const tagRx = /NSKIPC::RET::(.*)\n/
      //   const match = tagRx.exec(data);
      //   if(match) {
      //     const reto = JSON.parse(match[1]);
      //     resolve(reto);
      //   }
      // });
      //
      // kcli.on('close', (code) => {
      //   resolve('finished doing the thing');
      // });
      //
      // const ipcrq = {
      //   method: 'scan_file',
      //   args: {
      //     file: '/home/nelsk/Pictures/ss.png'
      //   }
      // };
      //
      // kcli.stdin.write(JSON.stringify(ipcrq) + '\n');
    });
  }
}
