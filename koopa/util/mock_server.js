'use strict';

const data = [
  {id: 1, author: "Erik Nelson", text: "This is the first comment"},
  {id: 2, author: "James Nelson", text: "This is another comment"},
  {id: 3, author: "Duder Nelson", text: "This is even more text"}
];

let nextId = 4;

const server = {
  getData: function (url) {
    console.log(`Mocked server hitting url: ${url}`);

    return new Promise(function(resolve, reject) {
      setTimeout(function () {
        resolve(data);
      }, 5);
    });
  },
  postData: function(newComment) {
    return new Promise(function(resolve, reject) {
      data.push({
        id: nextId,
        author: newComment.author,
        text: newComment.text
      });
      nextId++;
      resolve(data);
    });
  }
};

export default server;
