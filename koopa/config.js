'use strict';

const path = require('path');

const projectRoot = path.join(__dirname, '..');

const config = {
  projectRoot: projectRoot,
  kcli_exe: path.join(projectRoot, 'kuribo', 'build', 'kuribo-cli')
};

module.exports = config;
