var gulp = require('gulp');
var browserify = require('browserify');
var source = require('vinyl-source-stream');
var gutil = require('gulp-util');
var babelify = require('babelify');

// External deps you don't want to rebundle while developing
// but need to include in the application deployment
var dependencies = [
  'react',
  'react-dom'
];

// Keep count of the times a task has refired
var scriptsCount = 0;

//////////////////////////////////////////////////////////
// Gulp tasks
//////////////////////////////////////////////////////////
gulp.task('scripts', function () {
  bundleApp(false);
});

gulp.task('deploy', function () {
  bundleApp(true);
});

gulp.task('watch', function () {
  gulp.watch(['./koopa/**/*.js', './koopa/**/*.jsx'], ['scripts']);
});

gulp.task('default', ['scripts', 'watch']);

//////////////////////////////////////////////////////////
// Util
//////////////////////////////////////////////////////////

function bundleApp(isProduction) {
  scriptsCount++;
  // Browserify will bundle all our js files together in to one and will
  // let us use modules in the front end.
  var appBundler = browserify({
    entries: './koopa/app/app.jsx',
    debug: true
  });

  // If it's not for production, a separate vendor.js file will be
  // created the first time gulp is run so that we don't have to rebundle
  // things like react everytime there's a change in a js file
  if(!isProduction && scriptsCount === 1) {
    // Create vendor.js for dev env
    browserify({
      require: dependencies,
      debug: true
    })
    .bundle()
    .on('error', gutil.log)
    .pipe(source('vendor.js'))
    .pipe(gulp.dest('./dist/js'));
  }

  if(!isProduction) {
    // Make the deps external so they don't get bundled by the app
    // bundler. Dependencies are already bundled in the vendor.js for
    // the dev env.
    dependencies.forEach(dep => {
      appBundler.external(dep);
    });
  }

  appBundler.transform('babelify', { presets: ['es2015', 'react'] })
    .bundle()
    .on('error', gutil.log)
    .pipe(source('bundle.js'))
    .pipe(gulp.dest('./dist/js'));
}
